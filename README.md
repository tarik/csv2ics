# Synapses CSV to ICS (ical) converter

Uses the `icalendar` python package which can be installed on Debian derivatives with:

```
apt install python3-icalendar
```

or

```
pip install icalendar
```
